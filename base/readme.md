## Creating new OpenGL app

- create new windows applicaiton in VS (not console)
- set it up like on Image 1
- add backend.hpp (that file holds all init code for window creation and opengl init)
- add main.cpp (depending on the ANIMATE_BACKEND macro, different functions need to be defined)
- add folder with libraries, dlls and includes (/thirdparty/)
- in VS, go to Project > <projname> Properties...
    - in Configuration Properties
        - in VC++ directories (Image 2)
            - add "thirdparty" to Include Directories
            - add "thirdparty" to Library Directories
        - in Linker
            - in Input (Image 3)
                - add "opengl32.lib, glu32.lib, glew32.lib, glew32s.lib" to Additional Dependencies
            - in General (Image 4)
                - add "thirdparty" to Additional Library Directories

![s1](img/s1.png "s1")
![s2](img/s2.png "s2")
![s3](img/s3.png "s3")
![s4](img/s4.png "s4")