#pragma once

void InitApp();
void RenderApp();
void CleanupApp();

// loading graphics backend libraries
#include <Windows.h>
#include <stdio.h>
#include "thirdparty/glew.h"

//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
HINSTANCE   g_hInst = nullptr;
HWND        g_hWnd  = nullptr;
HGLRC       g_hRC   = nullptr;
HDC         g_hDC   = nullptr;

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HRESULT InitBackend(HWND hWnd);
void CleanupWindow();
void CleanupBackend(HWND hWnd);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void SwapBuffersBackend();

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // init
    if (FAILED(InitWindow(hInstance, nCmdShow)))
        return 0;

    if (FAILED(InitBackend(g_hWnd)))
    {
        CleanupBackend(g_hWnd);
        return 0;
    }

    ShowWindow(g_hWnd, SW_SHOW);
    SetForegroundWindow(g_hWnd);
    SetFocus(g_hWnd);

    InitApp();

    // main loop
    static bool quit = false;
    MSG msg = { 0 };
    while (!quit)
    {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        if (msg.message == WM_QUIT)
        {
            quit = true;
        }
        else
        {
            RenderApp();
            SwapBuffersBackend();
        }
    }

    // cleanup
    CleanupApp();
    CleanupBackend(g_hWnd);
    CleanupWindow();

    return (int)msg.wParam;
}

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = nullptr;
    wcex.lpszClassName = L"FrameWinClass";
    wcex.hIconSm = wcex.hIcon;
    if (!RegisterClassEx(&wcex))
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, 640, 480 };
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
    g_hWnd = CreateWindow(L"FrameWinClass", L"Frame",
        WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
        CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, hInstance,
        nullptr);

    if (!g_hWnd)
        return E_FAIL;

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Release resources, destroy window and unregister class
//--------------------------------------------------------------------------------------
void CleanupWindow()
{
    if (g_hWnd && !DestroyWindow(g_hWnd))
    {
        MessageBox(NULL, L"Could Not Release hWnd.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        g_hWnd = NULL;
    }

    if (!UnregisterClass(L"FrameWinClass", g_hInst))
    {
        MessageBox(NULL, L"Could Not Unregister Class.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        g_hInst = NULL;
    }
}

// --------------------------------------------------------------------------------------
// Called every time the application receives a message
//---------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)
    {
        case WM_CLOSE:
            PostQuitMessage(0);
            return 0;
        case WM_PAINT:
            hdc = BeginPaint(hWnd, &ps);
            EndPaint(hWnd, &ps);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

//--------------------------------------------------------------------------------------
// Initialize drawing API
//--------------------------------------------------------------------------------------
HRESULT InitBackend(HWND hWnd)
{
    if (!(g_hDC = GetDC(hWnd)))
    {
        printf("Error: Device fail.\n");
        return E_FAIL;
    }

    int format;
    static PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof(PIXELFORMATDESCRIPTOR), // Size Of This Pixel Format Descriptor
        1, // Version Number
        PFD_DRAW_TO_WINDOW | // Format Must Support Window
        PFD_SUPPORT_OPENGL | // Format Must Support OpenGL
        PFD_DOUBLEBUFFER, // Must Support Double Buffering
        PFD_TYPE_RGBA, // Request An RGBA Format
        32, // Select Our Color Depth
        0, 0, 0, 0, 0, 0, // Color Bits Ignored
        0, // No Alpha Buffer
        0, // Shift Bit Ignored
        0, // No Accumulation Buffer
        0, 0, 0, 0, // Accumulation Bits Ignored
        16, // 16Bit Z-Buffer (Depth Buffer)
        0, // No Stencil Buffer
        0, // No Auxiliary Buffer
        PFD_MAIN_PLANE, // Main Drawing Layer
        0, // Reserved
        0, 0, 0
    };

    if (!(format = ChoosePixelFormat(g_hDC, &pfd)))
    {
        printf("Error: PixelFormat fail.\n");
        return E_FAIL;
    }
    if (!SetPixelFormat(g_hDC, format, &pfd))
    {
        printf("Error: PixelFormat not set.\n");
        return E_FAIL;
    }
    if (!(g_hRC = wglCreateContext(g_hDC)))
    {
        printf("Error: Context not creted.\n");
        return E_FAIL;
    }
    if (!wglMakeCurrent(g_hDC, g_hRC))
    {
        printf("Error: Context not set.\n");
        return E_FAIL;
    }

    // initialize glew
    GLenum res = glewInit();
    if (res != GLEW_OK)
    {
        printf("Error: GLEW fail.\n");
        return E_FAIL;
    }

    return S_OK;
}

//--------------------------------------------------------------------------------------
// Release drawing API resources
//--------------------------------------------------------------------------------------
void CleanupBackend(HWND hWnd)
{
    if (g_hRC)
    {
        if (!wglMakeCurrent(NULL, NULL))
            MessageBox(NULL, L"Release Of DC And RC Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        if (!wglDeleteContext(g_hRC))
            MessageBox(NULL, L"Release Rendering Context Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        g_hRC = NULL;
    }

    if (g_hDC && !ReleaseDC(hWnd, g_hDC))
    {
        MessageBox(NULL, L"Release Device Context Failed.", L"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
        g_hDC = NULL;
    }
}

//--------------------------------------------------------------------------------------
// Swap buffers and display
//--------------------------------------------------------------------------------------
void SwapBuffersBackend()
{
    SwapBuffers(g_hDC);
}