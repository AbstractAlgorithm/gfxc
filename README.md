# todo

- light (photometry, photon&wave duality, spectrum)
- specular, diffuse, sss, rendering equation (Kajiya), brdf
- gpu pipeline, shaders
    - drivers and APIs
    - triangles
    - buffers, basic pipeline (non-programmable)
    - textures as data
    - attributes
    - uniforms (matrices)
    - fragments
    - interpolation
    - shaders (i/o), pipeline
- 3d math
- reflection, refraction, absorption (+ scattering)

- effects (consequence; making them separate for easier eval)
    - caustics
    - shadows
    - ambient occlusion
    - indirect illumination (color bleed)
    - volumetric lighting

- camera model (pinhole, realistic, film)
- camera effects
    - projection (orthographic, perspective)
    - fov + resolution
    - aberration (color shift)
    - bokeh dof
    - hdr & tonemapping
    - glow and bloom
    - motion blur
    - anti-aliasing
    - filmgrain
    - lens flare, glare

- basic app
    - win api
    - ogl init
    - drawing first triangle
    - adding light

- pipelines
    - forward
    - deferred
    - tiled
    - clustered

- strategies
    - rasterization
    - raytracing
    - reyes

- websites
    - http://www.lighthouse3d.com/
    - http://www.scratchapixel.com/
    - http://www.scratchapixel.com/old/
    - http://nehe.gamedev.net/
    - twitter
    - http://www.opengl-tutorial.org/
    - http://advances.realtimerendering.com/
    - http://blog.selfshadow.com/publications/s2015-shading-course/ (too specific)
    - gdc vault n stuff

- books
    - opengl programming guide 8th edition (red book)
    - glsl book (orange book)
    - programming massively parallel processors
    - pbrt
    - gpu gems (1,2,3)
    - real-time shadows
    - game engine gems
